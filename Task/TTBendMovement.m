//
//  TTBendMovement.m
//  TestTask
//
//  Created by Azat Zulkarnyaev on 30/03/15.
//  Copyright (c) 2015 Azat Zulkarnyaev. All rights reserved.
//

#import "TTBendMovement.h"

@implementation TTBendMovement
-(NSString *)descriptionForBodyPart:(TTBodyPart)bodyPart{
    if (bodyPart==TTBodyPartHead) {
        NSLog(@"Голова сгибаться не может!");
        return @"";
    }
    if (bodyPart== TTBodyPartChest) {
        return @"согнуто";
    }
    return @"согнуты";
}
@end
