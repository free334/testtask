//
//  TTPopDance.h
//  TestTask
//
//  Created by Azat Zulkarnyaev on 30/03/15.
//  Copyright (c) 2015 Azat Zulkarnyaev. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "TTDance.h"

@interface TTPopDance : NSObject <TTDance>

@end
