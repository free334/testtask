//
//  TTRythmMovement.m
//  TestTask
//
//  Created by Azat Zulkarnyaev on 30/03/15.
//  Copyright (c) 2015 Azat Zulkarnyaev. All rights reserved.
//

#import "TTRythmMovement.h"

@implementation TTRythmMovement
-(NSString *)descriptionForBodyPart:(TTBodyPart)bodyPart{
    if (bodyPart==TTBodyPartHead || bodyPart == TTBodyPartChest) {
        return @"двигается ритмично";
    }
    return @"двигаются ритмично";
}
@end
