//
//  TTPopDance.m
//  TestTask
//
//  Created by Azat Zulkarnyaev on 30/03/15.
//  Copyright (c) 2015 Azat Zulkarnyaev. All rights reserved.
//

#import "TTPopDance.h"
#import "TTSmoothMovement.h"
@implementation TTPopDance
-(NSString *)danceName{
    return @"Поп";
}
-(TTMusicType)musicType{
    return TTMusicTypePop;
}
-(id<TTMovement>)headMovement{
    return [TTSmoothMovement new];
}
-(id<TTMovement>)bodyMovement{
    return [TTSmoothMovement new];
}
-(id<TTMovement>)handsMovement{
    return [TTSmoothMovement new];
}
-(id<TTMovement>)legsMovement{
    return [TTSmoothMovement new];
}
@end
