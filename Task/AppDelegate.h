//
//  AppDelegate.h
//  Task
//
//  Created by Azat Zulkarnyaev on 31/03/15.
//  Copyright (c) 2015 Azat Zulkarnyaev. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

