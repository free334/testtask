//
//  TTElectroDance.m
//  TestTask
//
//  Created by Azat Zulkarnyaev on 30/03/15.
//  Copyright (c) 2015 Azat Zulkarnyaev. All rights reserved.
//

#import "TTElectroDance.h"
#import "TTBackAndForthMovement.h"
#import "TTCircleMovement.h"
#import "TTStanding.h"
#import "TTRythmMovement.h"
@implementation TTElectroDance
-(NSString *)danceName{
    return @"Electrodance";
}
-(id<TTMovement>)bodyMovement{
    return [TTBackAndForthMovement new];
}
-(id<TTMovement>)handsMovement{
    return [TTCircleMovement new];
}
-(id<TTMovement>)legsMovement{
    return [TTRythmMovement new];
}
-(id<TTMovement>)headMovement{
    return [TTStanding new];
}
-(TTMusicType)musicType{
    return TTMusicTypeElectrohouse;
}
@end
