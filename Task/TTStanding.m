//
//  TTMovements.m
//  TestTask
//
//  Created by Azat Zulkarnyaev on 30/03/15.
//  Copyright (c) 2015 Azat Zulkarnyaev. All rights reserved.
//

#import "TTStanding.h"
@implementation TTStanding
-(NSString *)descriptionForBodyPart:(TTBodyPart)bodyPart{
    if (bodyPart==TTBodyPartHead || bodyPart == TTBodyPartChest) {
        return @"не двигается";
    }
    return @"не двигаются";
}
@end
