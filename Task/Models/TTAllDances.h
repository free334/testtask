//
//  TTAllDances.h
//  Task
//
//  Created by Azat Zulkarnyaev on 31/03/15.
//  Copyright (c) 2015 Azat Zulkarnyaev. All rights reserved.
//

#ifndef Task_TTAllDances_h
#define Task_TTAllDances_h
#import "TTElectroDance.h"
#import "TTHipHopDance.h"
#import "TTHouseDance.h"
#import "TTPopDance.h"
#import "TTRnbDance.h"
#endif
