//
//  ViewController.m
//  Task
//
//  Created by Azat Zulkarnyaev on 31/03/15.
//  Copyright (c) 2015 Azat Zulkarnyaev. All rights reserved.
//

#import "ViewController.h"
#import "TTDancePool.h"
#import "TTAllDances.h"
@interface ViewController (){
    TTDancePool * dancePool;
}

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    TTDancer * dancer1=[[TTDancer alloc] initWithName:@"Танцор 1" dances:@[[TTHipHopDance new], [TTPopDance new]]];
    TTDancer * dancer2=[[TTDancer alloc] initWithName:@"Танцор 2" dances:@[]];
    TTDancer * dancer3=[[TTDancer alloc] initWithName:@"Танцор 3" dances:@[[TTHipHopDance new], [TTPopDance new], [TTElectroDance new]]];
    dancePool = [[TTDancePool alloc] initWithDancers:@[dancer1, dancer2, dancer3]];
    dancePool.music=TTMusicTypePop;
}
- (IBAction)chaneMusicToRnb:(id)sender {
    dancePool.music=TTMusicTypeRnb;
}
- (IBAction)changeMusicToElectro:(id)sender {
    dancePool.music=TTMusicTypeElectrohouse;
}
- (IBAction)changeMusicToPop:(id)sender {
    dancePool.music=TTMusicTypePop;
}
@end
