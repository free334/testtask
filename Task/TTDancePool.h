//
//  TTDancePool.h
//  TestTask
//
//  Created by Azat Zulkarnyaev on 30/03/15.
//  Copyright (c) 2015 Azat Zulkarnyaev. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "TTDance.h"
#import "TTDancer.h"
@interface TTDancePool : NSObject
-(instancetype)initWithDancers:(NSArray*) dancers;
@property (nonatomic,readonly) NSArray * dancers;
@property (nonatomic) TTMusicType music;
-(void) addDancer:(TTDancer*) dancer;
@end
