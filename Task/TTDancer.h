//
//  TTDancer.h
//  TestTask
//
//  Created by Azat Zulkarnyaev on 30/03/15.
//  Copyright (c) 2015 Azat Zulkarnyaev. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "TTMusic.h"
typedef enum {
    TTBodyPartHead,
    TTBodyPartLegs,
    TTBodyPartHands,
    TTBodyPartChest
}TTBodyPart;
@interface TTDancer : NSObject
@property (nonatomic, strong) NSString * name;

-(instancetype)initWithName:(NSString*) name dances:(NSArray*) dances;
-(BOOL) dance:(TTMusicType) musicType;
-(void) goDrinkVodka;
@end
