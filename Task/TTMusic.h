//
//  TTMusic.h
//  TestTask
//
//  Created by Azat Zulkarnyaev on 30/03/15.
//  Copyright (c) 2015 Azat Zulkarnyaev. All rights reserved.
//

#import <Foundation/Foundation.h>
typedef enum {
    TTMusicTypeRnb,
    TTMusicTypeElectrohouse,
    TTMusicTypePop
}TTMusicType;

@interface TTMusic : NSObject
+(NSString*) musicName:(TTMusicType) musicType;
@end
