//
//  TTBackAndForthMovement.m
//  TestTask
//
//  Created by Azat Zulkarnyaev on 30/03/15.
//  Copyright (c) 2015 Azat Zulkarnyaev. All rights reserved.
//

#import "TTBackAndForthMovement.h"

@implementation TTBackAndForthMovement
-(NSString *)descriptionForBodyPart:(TTBodyPart)bodyPart{
    if (bodyPart==TTBodyPartHead || bodyPart == TTBodyPartChest) {
        return @"двигается назад-вперед";
    }
    return @"двигаются назад-вперед";
}
@end

