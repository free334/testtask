//
//  TTHipHopDance.m
//  TestTask
//
//  Created by Azat Zulkarnyaev on 30/03/15.
//  Copyright (c) 2015 Azat Zulkarnyaev. All rights reserved.
//

#import "TTHipHopDance.h"
#import "TTBackAndForthMovement.h"
#import "TTBendMovement.h"
@implementation TTHipHopDance
-(NSString *)danceName{
    return @"Хип-хоп";
}
-(id<TTMovement>)bodyMovement{
    return [TTBackAndForthMovement new];
}
-(id<TTMovement>)legsMovement{
    return [TTBendMovement new];
}
-(id<TTMovement>)handsMovement{
    return [TTBendMovement new];
}
-(id<TTMovement>)headMovement{
    return [TTBackAndForthMovement new];
}
-(TTMusicType)musicType{
    return TTMusicTypeRnb;
}
@end
