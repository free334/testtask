//
//  TTDance.h
//  TestTask
//
//  Created by Azat Zulkarnyaev on 30/03/15.
//  Copyright (c) 2015 Azat Zulkarnyaev. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "TTMovement.h"
#import "TTMusic.h"

@protocol TTDance <NSObject>
-(TTMusicType) musicType;
-(id<TTMovement>) headMovement;
-(id<TTMovement>) legsMovement;
-(id<TTMovement>) handsMovement;
-(id<TTMovement>) bodyMovement;
-(NSString*) danceName;
@end
