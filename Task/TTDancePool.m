//
//  TTDancePool.m
//  TestTask
//
//  Created by Azat Zulkarnyaev on 30/03/15.
//  Copyright (c) 2015 Azat Zulkarnyaev. All rights reserved.
//

#import "TTDancePool.h"

@implementation TTDancePool
-(instancetype)initWithDancers:(NSArray*) dancers{
    self=[super init];
    if (self) {
        _dancers=dancers;
    }
    return self;
}
-(instancetype)init{
    self=[super init];
    if (self) {
        _dancers=[NSArray new];
    }
    return self;
}
-(void)addDancer:(TTDancer *)dancer{
    NSMutableArray * arr=[self.dancers mutableCopy];
    [arr addObject:dancer];
    _dancers=arr.copy;
}
-(void)setMusic:(TTMusicType)music{
    _music=music;
    NSLog(@"В клубе музыка сменилась на %@", [TTMusic musicName:music]);
    NSLog(@"-------------------------------");
    for (TTDancer * dancer in self.dancers) {
        if(![dancer dance:music]){
            [dancer goDrinkVodka];
        }
    }
}
@end
