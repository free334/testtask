//
//  TTDancer.m
//  TestTask
//
//  Created by Azat Zulkarnyaev on 30/03/15.
//  Copyright (c) 2015 Azat Zulkarnyaev. All rights reserved.
//

#import "TTDancer.h"
#import "TTDance.h"
#import "TTMovement.h"
@interface TTDancer()
@property (nonatomic, strong) NSArray * dances;
@end
@implementation TTDancer
-(instancetype)initWithName:(NSString*) name dances:(NSArray*) dances{
    self=[super init];
    if (self) {
        self.dances=dances;
        self.name=name;
    }
    return self;
}
-(BOOL) dance:(TTMusicType)musicType{
    BOOL canDance=NO;
    for (id<TTDance> dance in self.dances) {
        if ([dance musicType]==musicType) {
            canDance=YES;
            [self moveBody:dance];
            break;
        }
    }
    return canDance;
}
-(void) moveBody:(id<TTDance>) dance{
    NSLog(@"%@ танцует %@!", self.name, [dance danceName]);
    NSLog  (@"У него голова  %@",[[dance headMovement] descriptionForBodyPart:TTBodyPartHead]);
    NSLog(@"ноги %@",[[dance legsMovement] descriptionForBodyPart:TTBodyPartLegs]);
    NSLog(@"руки %@",[[dance handsMovement] descriptionForBodyPart:TTBodyPartHands]);
    NSLog (@"тело %@",[[dance bodyMovement] descriptionForBodyPart:TTBodyPartChest]);
    NSLog(@"----------------------------------------");
    
}
-(void) goDrinkVodka{
    NSLog(@"%@ пошел пить водку", self.name);
    NSLog(@"----------------------------------------");

}
@end
