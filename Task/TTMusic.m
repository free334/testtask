//
//  TTMusic.m
//  TestTask
//
//  Created by Azat Zulkarnyaev on 30/03/15.
//  Copyright (c) 2015 Azat Zulkarnyaev. All rights reserved.
//

#import "TTMusic.h"

@implementation TTMusic
+(NSString *)musicName:(TTMusicType)musicType{
    switch (musicType) {
        case TTMusicTypePop:
            return @"Поп";
        case TTMusicTypeElectrohouse:
            return @"Electrohouse";
        case TTMusicTypeRnb:
            return @"Rnb";
            
        default:
            break;
    }
}
@end
